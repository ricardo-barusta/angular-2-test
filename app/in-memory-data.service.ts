export class InMemoryDataService {
  createDb() {
    let heroes = [
      { id: 11, name: 'El santo' },
      { id: 12, name: 'Blue Demon' },
      { id: 13, name: 'Carlos Azaustre' },
      { id: 14, name: 'Chicharito' },
      { id: 15, name: 'John Papa' },
      { id: 16, name: 'Batman' },
      { id: 17, name: 'Flash' },
      { id: 18, name: 'Kaliman' }
    ];
    return { heroes };
  }
}
